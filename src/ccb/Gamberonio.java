package ccb;

public class Gamberonio implements Divisa{
    double vx;
    double vy;
    double vz;

    @Override
    public void sommati(Divisa d) {
        vx = Math.sqrt(((Gamberonio)d).vx+((Gamberonio)d).vy+((Gamberonio)d).vz);
        vy = Math.sqrt(((Gamberonio)d).vy+((Gamberonio)d).vx+((Gamberonio)d).vz);
        vz = Math.sqrt(((Gamberonio)d).vz+((Gamberonio)d).vy+((Gamberonio)d).vz);
    }

    @Override
    public void sottraiti(Divisa d) {
        vx = Math.sqrt(((Gamberonio)d).vx-((Gamberonio)d).vy-((Gamberonio)d).vz);
        vy = Math.sqrt(((Gamberonio)d).vz-((Gamberonio)d).vy-((Gamberonio)d).vz);
        vz = Math.sqrt(((Gamberonio)d).vx-((Gamberonio)d).vy-((Gamberonio)d).vz);
    }

    @Override
    public void stampati() {
            System.out.println("GMB " + (vx*vy*vz));
    }
    
    
}
