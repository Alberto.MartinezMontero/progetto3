package ccb;

public class CCB {

    public static void main(String[] args) {
        // Conto corrente in lire
        ContoCorrenteBancario clire = new ContoCorrenteBancario(new Lire());
        Lire l1 = new Lire();
        l1.valore = 1_000;
        clire.versamento(l1);
        Lire l2 = new Lire();
        l2.valore = 300;
        clire.prelievo(l2);
        clire.stampaSaldo();
        
        // Conto corrente in Euro
        ContoCorrenteBancario ceuro = new ContoCorrenteBancario(new Euro());
        Euro e1 = new Euro();
        e1.ve = 3_000.22;
        ceuro.versamento(e1);
        Euro e2 = new Euro();
        e2.ve = 1_300.33;
        ceuro.prelievo(e2);
        ceuro.stampaSaldo();
        
        // Conto corrente in Pizze Di Fango
        ContoCorrenteBancario cpizze = new ContoCorrenteBancario(new PizzaDiFango());
        PizzaDiFango p1 = new PizzaDiFango();
        p1.pdf = "*****";
        cpizze.versamento(p1);
        PizzaDiFango p2 = new PizzaDiFango();
        p2.pdf = "**";
        cpizze.prelievo(p2);
        cpizze.stampaSaldo();
        
        // Conto corrente in Gamberonio
        ContoCorrenteBancario cgamb = new ContoCorrenteBancario(new Gamberonio());
        Gamberonio g1 = new Gamberonio();
        g1.vx = 15;
        cgamb.versamento(g1);
        Gamberonio g2 = new Gamberonio();
        g2.vz = 45;
        cgamb.prelievo(g2);
        cgamb.stampaSaldo();
        
        ContoCorrenteBancario maga = new ContoCorrenteBancario(new Prodotto());
        Prodotto a1 = new Prodotto();
        a1.valore = 10_000;
        maga.versamento(a1);
                
    }

}
