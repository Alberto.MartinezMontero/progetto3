package ccb;

public interface Divisa {
    public void sommati(Divisa d);
    public void sottraiti(Divisa d);
    public void stampati();
}
