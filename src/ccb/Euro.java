package ccb;

public class Euro implements Divisa {
    public double ve;

    @Override
    public void stampati() {
        System.out.println("EURO " + ve);
    }

    @Override
    public void sottraiti(Divisa elle) {
        ve = ve - ((Euro)elle).ve;
    }

    @Override
    public void sommati(Divisa elle) {
        ve = ve + ((Euro)elle).ve;
    }
    
    
}
