package ccb;

public class PizzaDiFango implements Divisa {

    public String pdf = "";

    @Override
    public void sommati(Divisa d) {
        pdf += ((PizzaDiFango) d).pdf;
    }

    @Override
    public void sottraiti(Divisa d) {
        pdf = pdf.substring(((PizzaDiFango) d).pdf.length());
    }

    @Override
    public void stampati() {
        System.out.println("PDF " + pdf);
    }

}
