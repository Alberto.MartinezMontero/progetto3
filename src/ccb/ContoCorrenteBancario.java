package ccb;

public class ContoCorrenteBancario {

    public Divisa saldo;

    public ContoCorrenteBancario(Divisa saldo) {
        this.saldo = saldo;
    }
    
    public void versamento(Divisa v) {
//        saldo.valore = saldo.valore + v.valore;
        saldo.sommati(v);
    }

    public void prelievo(Divisa p) {
//        saldo.valore = saldo.valore - p.valore;
        saldo.sottraiti(p);
    }

    public void stampaSaldo() {
//        System.out.println("LIT " + saldo.valore);
        saldo.stampati();
    }

}
