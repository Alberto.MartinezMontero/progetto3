package ccb;

public class Lire implements Divisa {

    public int valore;

    public void sommati(Divisa elle) {
        valore = valore + ((Lire) elle).valore;
    }

    public void sottraiti(Divisa elle) {
        valore = valore - ((Lire) elle).valore;
    }

    public void stampati() {
        System.out.println("LIT " + valore);
    }
}
